#include <stdio.h>
#include <ctype.h>

int main(void)
{
    int wordcount = 0; // 총 단어 개수
    int ulwdcount = 0; // 대문자로 시작해 소문자로 끝나는 단어 개수

    // 문제풀이 시작
    char c; // 입력 문자
    char a; // 시작 문자
    char z; // 끝 문자
    int space = 1; // 직전 문자가 공백

    while((c = getchar()) != EOF) {
        if (isspace(c)) { // 공백 입력
            if (!space) { // 이전에 공백이 아니였으면
                if ('A' <= a && a <= 'Z' && 'a' <= z && z <= 'z') {
                    ulwdcount++; //
                    a = z = 0;
                }
                space = 1; // 직전 문자는 공백
            }
        } else { // 문자 입력
            if (space) { // 직전에 공백이었으면
                wordcount++; // 단어 수 증가
                a = c; // 시작 문자
                space = 0; // 직전 문자 공백 아님
            } else { // 첫번째 문자가 아니면
                z = c; // 끝 문자
            }
        }
    }
    // 문제 풀이 끝

    printf("%d\n", wordcount); // 전체 단어 개수
    printf("%d\n", ulwdcount); // 첫글자 대문자이고 마지막 글자 소문자인 단어 개수

    return 0;
}
